﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationStateController : MonoBehaviour
{

    Animator animator;
    float velocity = 0.0f;
    public float acceleration = 0.1f;
    public float desacceleration=0.5f;
    int VelocityHash;
    int  backHash;
    
    void Start()
    {
        animator = GetComponent<Animator>();


        VelocityHash = Animator.StringToHash("Velocity");
        backHash = Animator.StringToHash("Back ");
    }

      
    void Update()
    {
        bool forwardPressed = Input.GetKey("w");
        bool runPressed = Input.GetKey("left shift");
        bool backPressed = Input.GetKey("s");

        if (forwardPressed && velocity<1.0f)
        {
            velocity += Time.deltaTime * acceleration;
        }

        if (!forwardPressed && velocity > 0.0f)
        {
            velocity -= Time.deltaTime * desacceleration;
        }

        if (!forwardPressed && velocity <0.0f)
        {
            velocity=0.0f;
        }

        animator.SetFloat(VelocityHash, velocity);

        if (backPressed)
        {
            animator.SetBool(backHash, true);
        }
        if (!backPressed)
        {
            animator.SetBool(backHash, false);
        }

    }
}
